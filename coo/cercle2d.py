"""Définition de la classe Cercle2D"""
from coo.forme import Forme


class Cercle2D(Forme):
    """Représente un cercle en 2 dimensions"""

    def __init__(self, centre, rayon):
        """Initialise un cercle à partir d'un point et d'un rayon"""
        self.centre = centre
        self.rayon = rayon

    def __str__(self):
        return "Cercle2D({}, {})".format(self.centre, self.rayon)

    def translate(self, dx, dy):
        """Déplace le cercle"""
        self.centre.translate(dx, dy)
